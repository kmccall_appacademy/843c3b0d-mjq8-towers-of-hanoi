# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi

  attr_reader :towers


  def initialize
    @towers = [[3,2,1],[],[]]
  end

 def play
    get_move until won?
  end

  def get_move
    render
    puts "What pile do you want to select a disc from? (0, 1, or 2?)"
    from_tower = gets.chomp.to_i
    puts "What pile do you want to move the disc to? (0, 1, or 2?)"
    to_tower = gets.chomp.to_i

    if valid_move?(from_tower, to_tower)
      move(from_tower, to_tower)
    else
      puts "That is not a valid move!!"
      play
    end
  end

  def render
    puts "------"
    puts towers[0].reverse
    puts "------"
    puts towers[1].reverse
    puts "------"
    puts towers[2].reverse
    puts "------"
  end

  def won?
    if towers == [[],[],[3,2,1]] || towers == [[],[3,2,1],[]]
      puts "YOU WIN!"
      render
      exit
    end
    return false
  end

  def valid_move?(from_tower, to_tower)
    return false if from_tower > 2 || from_tower < 0
    return false if to_tower > 2 || to_tower < 0
    if towers[from_tower][-1]
      if towers[to_tower][-1]
        towers[from_tower][-1].to_i < towers[to_tower][-1].to_i
      else
        true
      end
    else
      false
    end
  end

  def move(from_tower, to_tower)
    disc = towers[from_tower].pop
    towers[to_tower].push(disc)
  end

end


if __FILE__ == $PROGRAM_NAME
  t = TowersOfHanoi.new
  t.play
end
